/* **********************************************************************************************************
	Channel Capacity Playground

	Author: Alfredo Metere
	e-mail: metal@icsi.berkeley.edu or metere1@llnl.gov
	
	Description:
		This playground assumes that there is a dynamical system producing a noiseless signal input 
                by the user as a command line argument, over a fixed sampling rate. 
                It then calculates the channel capacity needed to transport such signal if the signal is 
		either encoded as a real number, a floating point number and a fixed point number.

		The program will give error if the chosen signal value is too big to be stored or manipulated
		by double precision floating point values.


   ********************************************************************************************************** */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

double log2(double a)
{
	return log(a)/log(2.0);
}

double pow2(double a)
{
	return pow(2.0,a);
}

int main(int argc, char ** argv)
{

	FILE *fp = fopen("out.csv","w");

	typedef union FVal
	{
		double d;
		uint64_t i;
	} fval;
	
	
	if (argc != 5) 
	{
		printf("Usage: %s [fixprecision] [signal_value] [exp_bits] [mantissa bits]\n", argv[0]);
		printf("Example: %s 1.E-5 3.232143545 10 20\n", argv[0]);
		exit(-1);
	}

	if (atof(argv[1]) <=0 || atof(argv[1]) > 1)
	{
		printf("Precision must be within the interval (0,1] \t Exiting ...\n");
		exit(-1);
	}

	uint64_t exp_bits = atoi(argv[3]); // WIP
	uint64_t m_bits   = atoi(argv[4]); // WIP

	double fixprec    = atof(argv[1]); // 1.E-4; // Fixed point precision
	double signal     = atof(argv[2]); // Noiseless signal
	printf("Signal = %g\n", signal);

	double fixsignal = (int) (signal/fixprec);
	printf("Fixed point signal: %.0f \t Precision: %e\n",fixsignal, fixprec);

	double sb = fabs(signal)/signal+1.0;                               // Sign bit. 0 = negative, 1 = positive.

	double sE = fabs(log2(1.0+signal))/(log2(1.0+signal))+1.0;               // Exponent sign
	printf("sE = %.15f\n",sE);
	double  E = floor(log2(1.0+fabs(signal)));
	fval M;                                                          // Mantissa
	// M.d = signal/pow2(pow(-1,sE)*E);
	M.d = fabs(signal)/pow2(E);

//	double vf = pow2(pow(-1,sE)*E) * M.d;                         // Reconstructed signal
	double vf = pow2(E) * M.d;
	printf("E = %.15f\t M = %.15f\t = %ld \n", E, M.d, M.i);                  // Prints exponent bits and mantissa
	printf("Signal verification = %.15f\n",vf);          // Checks that everything is done correctly

	double fpccap  = E + ceil(log2(1.0+M.d));
	double ccap    = ceil(log2(1.0+fabs(fixsignal)));
	
	printf("Floating point channel capacity per time step: %g bits\n",fpccap);
	printf("   Fixed point channel capacity per time step: %g bits\n",ccap);

	for (int i = 0; i < 1048576; i++)
	{
		signal = (double) i/1048676.0*1000.0;
		E = floor(log2(signal));
		M.d = signal/pow2(E);
		ccap = E + ceil(log2(M.d));
		fprintf(fp, "%.16f,%.16f,%.16f,%.16f\n",signal,ccap,E,ceil(log2((double) M.i)));
	}
	fclose(fp);
	return(0);
}
