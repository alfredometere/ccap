CC = gcc
CFLAGS = -S

LD = gcc
LDLIBS = -lm

CPP = cpp
ASM = as

SRC = ccapfloat.c # Source code
PRC = ccapfloat.i # Preprocessed code
ARC = ccapfloat.s # Assembly code
OBJ = ccapfloat.o # Object code
TRG = ccapfloat   # Executable


default: all

all: clean fpccap

fpccap:
	$(CPP) -v $(SRC) > $(PRC)
	$(CC)  -v -S $(PRC)
	$(ASM) -v -o $(OBJ) $(ARC)
	$(LD)  -v -o $(TRG) $(OBJ) $(LDLIBS)

clean:
	rm -rf $(OBJ) $(PRC) $(ARC) $(TRG) 
