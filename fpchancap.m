clear
format long

Signal =  [1:0.5:10000000];

SignalSignBit = abs(Signal)./Signal;
SignalExponent = floor(log2(abs(Signal))); % This value is in bits. To have exact bitmask match you must add 1023 as offset
SignalMantissa = abs(Signal)./2.^SignalExponent; % This value is a number within the range [1, 2).
SignalMantissaAsUINT64 = (SignalMantissa - 1).*2^52; % This transforms the mantissa from double to UINT64 and removes the leading 1
BitsUsedByMantissa = ceil(log2(SignalMantissaAsUINT64)); % Channel Capacity

% Let's calculate extracting the actual bit values for control.
ControlSignal = dec2bin(typecast(Signal,'uint64')); % Straight conversion from double to UINT64
%ControlSignalExponent = ControlSignal(:,2:12); % For signal ranges starting with negative numbers
 ControlSignalExponent = ControlSignal(:,1:11); % For positive signal ranges
%ControlSignalMantissa = ControlSignal(:,13:64); % For signal ranges starting with negative numbers
 ControlSignalMantissa = ControlSignal(:,12:63); % For positive signal ranges
ControlSignalMantissaAsUINT64 = bin2dec(ControlSignalMantissa);

ErrorMantissa = (SignalMantissaAsUINT64.'-ControlSignalMantissaAsUINT64)/2^52;

% Plots the calculated mantissa vs. the control mantissa.
% plot(Signal,SignalMantissaAsUINT64./2^52,'x',Signal,ControlSignalMantissaAsUINT64./2^52,'o')

% Plots the error on representing the mantissa analytically
% plot(Signal,ErrorMantissa,'o')

semilogx(Signal,BitsUsedByMantissa,'.')

%%% TODO: Embellish and refine the plots.